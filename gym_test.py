# Print all registered ROMs
"""
import ale_py.roms as roms
print(roms.__all__)
"""

import gym

env = gym.make("SpaceInvaders")
env.reset()

for _ in range(1000):
    env.render()
    env.step(env.action_space.sample()) # take a random action

env.close()

